
import { BrowserRouter, Route, Routes } from "react-router-dom";
import RegisterFormPage from '../page/RegisterFormPage'
import './App.css'
import Header from './component/Header'
import EmailConfirmSuccessPage from '../page/EmailConfirmSuccessPage'

function App() {

  return (
    <BrowserRouter>
      <Header/>
        <Routes>
          <Route path="/register" Component={RegisterFormPage} />
          <Route path="/EmailConfirmSuccess" Component={EmailConfirmSuccessPage} />
        </Routes>
    </BrowserRouter>
  )
}

export default App
