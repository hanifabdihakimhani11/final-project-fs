import React from 'react';
import { Button, TextField, Container, Typography, Grid, Box } from '@mui/material';
import GambarEmailSuccess from '../assets/GambarEmailConfirmSuccess.png'

function EmailConfirmSuccess() {
    return (
        <>
        <Container maxWidth="md" style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', paddingTop: '40px' }}>
          
          <Grid container direction="column" spacing={3} alignItems="center">

            <Grid item>
              <img src={GambarEmailSuccess} alt="Email Confirm Success" style={{ maxWidth: '200px', height: 'auto' }} />
            </Grid>
    
            <Grid item>
              <Typography variant='h4' align='center'>
                Email Confirmation Success
              </Typography>
            </Grid>
    
            <Grid item>
              <Typography align='center'>
                Your email already! Please login first to access the web
              </Typography>
            </Grid>
    
            <Grid item>
              <Button variant='contained' color="primary">
                Login Here
              </Button>
            </Grid>
          </Grid>
        </Container>
        </>
      );
}

export default EmailConfirmSuccess;
