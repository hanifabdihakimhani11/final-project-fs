import { useState } from 'react';
import customColors from "./Colors";
import { Button, TextField, Container, Typography, Grid, Box } from '@mui/material';

function RegistrationForm() {
    const loginColor = customColors.palette.ochre.loginColor;
    const [formData, setFormData] = useState({
        name: '',
        email: '',
        password: '',
        confirmPassword: ''
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
    };

    return (
        <Container maxWidth="md">
            <Typography variant="h4" align="left">
                Let's Join our course!
            </Typography>
            <Typography variant="h5" align="left" mb={6} mt={2}>
                Please register first
            </Typography>
            <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            fullWidth
                            size='small'
                            label="Name"
                            variant="outlined"
                            name="name"
                            value={formData.name}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            fullWidth
                            size='small'
                            label="Email"
                            variant="outlined"
                            name="email"
                            value={formData.email}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            fullWidth
                            size='small'
                            label="Password"
                            variant="outlined"
                            type="password"
                            name="password"
                            value={formData.password}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            fullWidth
                            size='small'
                            label="Confirm Password"
                            variant="outlined"
                            type="password"
                            name="confirmPassword"
                            value={formData.confirmPassword}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Box textAlign="right" mt={2}>
                            <Button
                                type="submit"
                                variant="contained"
                                style={{backgroundColor: loginColor, padding: '7px 40px', borderRadius: '10px'}}
                            >
                                Sign Up
                            </Button>
                        </Box>
                    </Grid>
                </Grid>
            </form>
            <Typography align="center" mt={12}>
                Have an account? <Button color='secondary'>Login here</Button>
            </Typography>
        </Container>
    );
}

export default RegistrationForm;
