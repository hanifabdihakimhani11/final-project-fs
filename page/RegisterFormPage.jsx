import Header from '../src/component/Header'
import RegisterForm from '../src/component/RegisterForm'

function RegisterFormPage() {

  return (
    <>
      <Header />
      <RegisterForm />
    </>
  )
}

export default RegisterFormPage;
