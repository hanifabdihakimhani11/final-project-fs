
import EmailConfirmSuccess from '../src/component/EmailConfirmSuccess'
import HeaderEmailConfirmSuccess from '../src/component/HeaderEmailConfirmSuccess'

export default function EmailConfirmSuccessPage() {
    return (
        <>
            <HeaderEmailConfirmSuccess />
            <EmailConfirmSuccess />
        </>
    )
}